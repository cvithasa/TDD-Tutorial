from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers

from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper
import os
import json

response = {}
csui_helper = CSUIhelper()

def index(request):
    # Page halaman menampilkan list mahasiswa yang ada
    # TODO berikan akses token dari backend dengan menggunakaan helper yang ada
    mahasiswa_list = csui_helper.instance.get_mahasiswa_list()
    friend_list = Friend.objects.all()
    response = {"author" : "Rai Indhira", "mahasiswa_list": mahasiswa_list, "friend_list": friend_list}
    html = 'lab_7/lab_7.html'
    return render(request, html, response)

def friend_list(request):
    friend_list = Friend.objects.all()
    response['friend_list'] = friend_list
    response['author'] = "Rai Indhira"
    html = 'lab_7/daftar_teman.html'
    return render(request, html, response)

@csrf_exempt
def add_friend(request):
    if request.method == 'POST':
        name = request.POST['name']
        npm = request.POST['npm']
        alamat = request.POST['alamat']
        pos = request.POST['pos']
        kota = request.POST['kota']
        tanggal = request.POST['tanggal']
        is_already_friend = Friend.objects.filter(npm = npm).count()
        if (is_already_friend == 0):
            friend = Friend(friend_name=name, npm=npm, alamat=alamat, kode_pos=pos, kota_lahir=kota, tgl_lahir=tanggal)
            friend.save()
            data = model_to_dict(friend)
            return HttpResponse(data)

@csrf_exempt
def delete_friend(request):
    name = request.POST['name']
    npm = request.POST['npm']
    Friend.objects.filter(npm=npm).delete()
    return HttpResponseRedirect('/lab-7/get-friend-list')
    

@csrf_exempt
def validate_npm(request):
    npm = request.POST.get('npm', None)
    num_result = Friend.objects.filter(npm = npm).count()
    data = {
        'is_taken': (num_result > 0)  
    }
    return JsonResponse(data)

def model_to_dict(obj):
    data = serializers.serialize('json', [obj,])
    struct = json.loads(data)
    data = json.dumps(struct[0]["fields"])
    return data

@csrf_exempt
def friend_list_json(request):
    friend = Friend.objects.all()
    data = serializers.serialize("json", friend)
    return HttpResponse(data, content_type='application/json')

def next_mahasiswa_list(request):
    csui_helper.instance.change_direction_next_mahasiswa_list();
    return HttpResponseRedirect("/lab-7/")

def prev_mahasiswa_list(request):
    csui_helper.instance.change_direction_prev_mahasiswa_list();
    return HttpResponseRedirect("/lab-7/")
