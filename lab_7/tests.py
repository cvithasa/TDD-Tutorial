from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index

# Create your tests here.

class Lab7UnitTest(TestCase):
    def test_lab_7_url_is_exist(self):
        response = Client().get('/lab-7/')
        self.assertEqual(response.status_code, 200)

    def test_lab_6_is_using_index_page(self):
        found = resolve('/lab-7/')
        self.assertEqual(found.func, index)