from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index

class Lab6UnitTest(TestCase):
	def test_lab_6_url_is_exist(self):
		response = Client().get('/lab-6/')
		self.assertEqual(response.status_code, 200)
		
	def test_root_url_now_is_using_index_page_from_lab_6(self):
		found = resolve('/lab-6/')
		self.assertEqual(found.func, index)
